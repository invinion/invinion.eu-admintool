﻿using Jan18101997.Utility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace INVINION.GUI
{
    /// <summary>
    /// Interaktionslogik für Login.xaml
    /// </summary>
    ///
    internal class UserLoginList
    {
        public int id { get; set; }

        public bool admin { get; set; }
    }

    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            this.Title = "Login iNv AdminTool v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            if (Properties.Settings.Default.login_save == true)
            {
                user.Text = Properties.Settings.Default.login_User;
                password.Password = Properties.Settings.Default.login_Passwd;
                save.IsChecked = true;
            }
        }

        private bool allowExit = false;
        private List<UserLoginList> loginList = new List<UserLoginList>();

        private void vergessen_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://invinion.eu/user/?action=lostpwd");
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loginList.Clear();
                MySqlConnection connection_clan = new MySqlConnection(PrivateData.MySQLLogin.getclanMySqlLogin());
                MySqlCommand command_clan = connection_clan.CreateCommand();
                command_clan.CommandText = "SELECT pwd, id FROM clanusers WHERE user = '" + user.Text + "'";
                connection_clan.Open();
                MySqlConnection connection_admintool = new MySqlConnection(PrivateData.MySQLLogin.getadmintoolMySqlLogin());
                MySqlCommand command_admintool = connection_admintool.CreateCommand();
                command_admintool.CommandText = "SELECT id, admin FROM tool_login";
                connection_admintool.Open();
                MySqlDataReader Reader;
                Reader = command_admintool.ExecuteReader();
                while (Reader.Read())
                {
                    loginList.Add(new UserLoginList { id = int.Parse(Reader.GetValue(0).ToString()), admin = Convert.ToBoolean(int.Parse(Reader.GetValue(1).ToString())) });
                }
                Reader.Close();
                Reader = command_clan.ExecuteReader();
                Reader.Read();
                if (Reader.GetValue(0).ToString().Equals(Utility.GetMD5Hash(password.Password), StringComparison.OrdinalIgnoreCase))
                {
                    bool containsID = false;
                    bool allowDelete = false;

                    foreach (UserLoginList item in loginList)
                    {
                        if (item.id == int.Parse(Reader.GetValue(1).ToString()))
                        {
                            containsID = true;
                            allowDelete = item.admin;
                        }
                    }

                    if (containsID == true)
                    {
                        if (save.IsChecked == true)
                        {
                            Properties.Settings.Default.login_User = user.Text;
                            Properties.Settings.Default.login_Passwd = password.Password;
                            Properties.Settings.Default.login_save = true;
                        }
                        else
                        {
                            Properties.Settings.Default.login_User = "";
                            Properties.Settings.Default.login_Passwd = "";
                            Properties.Settings.Default.login_save = false;
                        }
                        Properties.Settings.Default.Save();
                        UserInfo.LoggedIn.setUID(int.Parse(Reader.GetValue(1).ToString()));
                        UserInfo.LoggedIn.setAllowDelete(allowDelete);

                        allowExit = true;
                        DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("Du hast nicht das benötigten Rechte! Du must als ClanLeader eingetragen seien!", "Falsche Level", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Login mit dem Namen \"" + user.Text + "\" und diesem Passwort nicht möglich!", "Falsche Angaben", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Die LoginDaten können nicht überprüft werden." + Environment.NewLine + "Fehler: " + ex, "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Login_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (allowExit == false)
            {
                DialogResult = false;
            }
        }

        private void Login_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                login_Click(this, new RoutedEventArgs());
            }
        }
    }
}