﻿using INVINION.Views;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace INVINION.GUI
{
    /// <summary>
    /// Interaktionslogik für Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings(MySqlConnector sql)
        {
            InitializeComponent();
            this.Title = "Einstellungen iNv AdminTool v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            welcomeScreen.IsChecked = Properties.Settings.Default.welcome_show;
            if (UserInfo.LoggedIn.getAllowDelete() == false)
            {
                rightsAdd.IsEnabled = false;
                rightsChange.IsEnabled = false;
                rightsRemove.IsEnabled = false;
            }
            data = sql;

            data.loadRights(rightsList);
        }

        private MySqlConnector data;

        //WPF Events
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.welcome_show = (bool)welcomeScreen.IsChecked;
            Properties.Settings.Default.Save();
        }

        //Click Events
        private void rightsAdd_Click(object sender, RoutedEventArgs e)
        {
            data.addRights(int.Parse(rightsID.Text), (bool)rightsAdmin.IsChecked);
            data.loadRights(rightsList);
        }

        private void rightsChange_Click(object sender, RoutedEventArgs e)
        {
            if (rightsList.SelectedIndex >= 0)
            {
                RightsView rv = (RightsView)rightsList.SelectedItem;
                data.changeRights(int.Parse(rv.id), (bool)rightsAdmin.IsChecked);
                data.loadRights(rightsList);
            }
        }

        private void rightsRemove_Click(object sender, RoutedEventArgs e)
        {
            if (rightsList.SelectedIndex >= 0)
            {
                RightsView rv = (RightsView)rightsList.SelectedItem;
                data.removeRights(int.Parse(rv.id));
                data.loadRights(rightsList);
            }
        }

        //Changed Events
        private void rightsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (rightsList.SelectedIndex >= 0)
            {
                RightsView rv = (RightsView)rightsList.SelectedItem;
                rightsID.Text = rv.id;
                rightsAdmin.IsChecked = General.convertYesNoToBool(rv.allowdelete);
            }
        }

        //Input Events
        private void CheckIfInt_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            string[] zeroTonine = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            if (zeroTonine.Contains(e.Text) == true)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}