﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace INVINION.GUI
{
    /// <summary>
    /// Interaktionslogik für IRCChat.xaml
    /// </summary>
    public partial class IRCChat : Window
    {
        public IRCChat(string name)
        {
            InitializeComponent();
            userInfo.Content = name + "@invinionadmintoolirc";
            irc.Source = new Uri("http://webchat.quakenet.org/?nick=" + name + "&channels=INVINION");
        }

        private bool allowExit = false;

        private void reload_Click(object sender, RoutedEventArgs e)
        {
            irc.Refresh();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (allowExit == false)
            {
                irc.Source = new Uri("http://invinion.eu/admintool/");
                e.Cancel = true;
            }
        }

        private void irc_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            if (irc.Source.ToString().Equals("http://invinion.eu/admintool/", StringComparison.OrdinalIgnoreCase))
            {
                allowExit = true;
                this.Close();
            }
        }

        public void HideScriptErrors(WebBrowser wb, bool Hide)
        {
            FieldInfo fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);

            if (fiComWebBrowser == null) return;

            object objComWebBrowser = fiComWebBrowser.GetValue(wb);

            if (objComWebBrowser == null) return;

            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });
        }

        private void irc_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            HideScriptErrors(irc, true);
        }
    }
}