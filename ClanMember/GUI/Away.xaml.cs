﻿using INVINION.Views;
using System.Windows;
using System.Windows.Controls;

namespace INVINION.GUI
{
    /// <summary>
    /// Interaktionslogik für Away.xaml
    /// </summary>
    public partial class Away : Window
    {
        public Away(MySqlConnector dataClass, int UID, string name)
        {
            InitializeComponent();
            this.Show();
            this.Topmost = true;
            this.Title = "Abwesenheit von " + name + " iNv AdminTool v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            awayName.Content = "Name: " + name + "(" + UID.ToString() + ")";
            awayMessage.NavigateToString("<html><body><h3><p> iNv AdminTool by Jan18101997 </p></h3></body></html>");

            data = dataClass;
            data.loadAway(awayList, UID);
        }

        private MySqlConnector data;

        //Changed Events
        private void awayList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (awayList.SelectedIndex >= 0)
            {
                AwayView av = (AwayView)awayList.SelectedItem;
                string message = data.getAwayReason(int.Parse(av.id));
                if (message.Length <= 3)
                {
                    message = "<html><body><h3><p> iNv AdminTool by Jan18101997 </p></h3></body></html>";
                }
                awayMessage.NavigateToString("<html><body>" + message + "</body></html>");
            }
            else
            {
                awayMessage.NavigateToString("<html><body><h3><p> iNv AdminTool by Jan18101997 </p></h3></body></html>");
            }
        }
    }
}