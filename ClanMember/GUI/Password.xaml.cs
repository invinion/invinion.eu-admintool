﻿using Jan18101997.Utility;
using System.Windows;

namespace INVINION.GUI
{
    /// <summary>
    /// Interaktionslogik für Password.xaml
    /// </summary>
    public partial class Password : Window
    {
        public Password(MySqlConnector sql, int user)
        {
            InitializeComponent();
            this.Title = "Password iNv AdminTool v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            data = sql;
            UID = user;
        }

        private MySqlConnector data;
        private int UID;

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            data.changeUserPassword(UID, Utility.GetMD5Hash(pwd.Text).ToLower());
            this.Close();
        }
    }
}