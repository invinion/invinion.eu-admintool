﻿using Jan18101997.Utility;
using System;
using System.Net.Mail;
using System.Windows;

namespace INVINION.GUI
{
    /// <summary>
    /// Interaktionslogik für Mail.xaml
    /// </summary>
    public partial class Mail : Window
    {
        public Mail(string mail)
        {
            InitializeComponent();
            this.Title = "Mail iNv AdminTool v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ToMail = mail;
            to.Content = "An: " + mail;
        }

        private string ToMail;

        private void send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(PrivateData.OtherLogin.getadmintoolMailLogin(PrivateData.MailData.Server));

                mail.From = new MailAddress(PrivateData.OtherLogin.getadmintoolMailLogin(PrivateData.MailData.From));
                mail.To.Add(ToMail);
                string MailSubject;
                if (subject.Text.Length >= 3)
                {
                    MailSubject = "[iNv] " + subject.Text;
                }
                else
                {
                    MailSubject = "[iNv] AdminToll-Message (Kein Betreff angegeben)";
                }
                mail.Subject = MailSubject;
                mail.Body = message.Text + Utility.NewLine(2) + "______________________________________________________________" + Environment.NewLine +
                                                                "Es kann nicht direkt auf diese Nachricht geantwortet werden!" + Environment.NewLine +
                                                                "Bitte wende dich an einen Admin!" + Utility.NewLine(2) +
                                                                "Dein Invinion.eu Admin-Team ;)";

                SmtpServer.Port = 25;
                SmtpServer.Credentials = new System.Net.NetworkCredential(PrivateData.OtherLogin.getadmintoolMailLogin(PrivateData.MailData.UserName), PrivateData.OtherLogin.getadmintoolMailLogin(PrivateData.MailData.Password));
                SmtpServer.EnableSsl = false;

                SmtpServer.Send(mail);
                MessageBox.Show("Mail gesendet an " + ToMail + "!", "Gesendet", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler beim Senden: " + ex.ToString(), "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}