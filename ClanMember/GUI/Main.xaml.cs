﻿using INVINION.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Imaging;

namespace INVINION
{
    /// <summary>
    /// Interaktionslogik für Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        public Main()
        {
            data = new MySqlConnector();
            InitializeComponent();
            this.Title = "iNv AdminTool by Jan18101997 v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            new Version.Updater();
            setStaticClasses();
            login();

            filterOwnText.Text = Properties.Settings.Default.search_Custom;
            userList.SelectionChanged += userList_SelectionChanged;
            clanTabs.SelectionChanged += clanTabs_SelectionChanged;
            userListLoad.Click += userListLoad_Click;
        }

        private MySqlConnector data;
        private int userListAndFilterLastTab;
        private List<SquadView> squadUserViewList = new List<SquadView>();

        //Methoden
        private void clearUserUI()
        {
            userDbID.Content = "DB ID: N/A";
            userNick.Content = "Nick: N/A";
            userName.Text = "N/A";
            userAge.Content = "Alter: N/A";
            userMail.Content = "E-Mail: N/A";
            userLastlogin.Content = "Letzter Login: N/A";
            userStats.Content = "Teilnahme: Ja: N/A Nein: N/A Entschuldigt: N/A Gesamt: N/A";
            userLevel.SelectedIndex = -1;
            userMeetingComment.Text = "";
            userCommentText.Text = "";
            userLevel.SelectedIndex = 0;
            profilePicjpg.Source = new BitmapImage(new Uri("http://invinion.eu/inc/images/uploads/useravatare/Nothing.nithing", UriKind.Absolute));
            profilePicpng.Source = new BitmapImage(new Uri("http://invinion.eu/inc/images/uploads/useravatare/Nothing.nithing", UriKind.Absolute));
            setMultiSelectUI(true);
            userSquadPosi.SelectedIndex = -1;

            userCommentList.Items.Clear();
            userMeeting.Items.Clear();
            userSquadList.Items.Clear();
            userSquadUserList.Items.Clear();
        }

        private bool containsComparison(string source, string cont, StringComparison compare)
        {
            return source.IndexOf(cont, compare) >= 0;
        }

        private void loadSquads()
        {
            userSquadList.Items.Clear();

            foreach (SquadView item in squadUserViewList)
            {
                userSquadList.Items.Add(item);
            }
        }

        private void login()
        {
            if (Debugger.IsAttached == true)
            {
                UserInfo.LoggedIn.setUID(3);
                UserInfo.LoggedIn.setUName("AdminToolUser");
                UserInfo.LoggedIn.setAllowDelete(true);
                data.addHistory("Debugging Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + " from " + Environment.MachineName);
            }
            else
            {
                GUI.Login login = new GUI.Login();
                if ((bool)login.ShowDialog() == false)
                {
                    Environment.Exit(0);
                }
                data.addHistory("User " + UserInfo.LoggedIn.getUID() + " logged in");
            }
            UserInfo.LoggedIn.setUName(data.getName(UserInfo.LoggedIn.getUID()));
            General.OutPut("Logged in as: id:" + UserInfo.LoggedIn.getUID().ToString() + " Name:" + UserInfo.LoggedIn.getUName() + " AllowDelete:" + UserInfo.LoggedIn.getAllowDelete().ToString());
        }

        private void setMultiSelectUI(bool multi)
        {
            userName.IsEnabled = multi;
            userNameChange.IsEnabled = multi;
            userLevel.IsEnabled = multi;
            userLevelChange.IsEnabled = multi;
            userLoadStats.IsEnabled = multi;
            userChangePassword.IsEnabled = multi;
            userCommentAdd.IsEnabled = multi;
            userCommentDelete.IsEnabled = multi;
            userCommentText.IsEnabled = multi;
            userCommentList.IsEnabled = multi;
            userMeetingChange.IsEnabled = multi;
            userMeetingRemove.IsEnabled = multi;
            userMeetingComment.IsEnabled = multi;
        }

        private void setStaticClasses()
        {
            General.setOutputBox(consoleOutput);
            General.setScrollViewer(output_scroll);
            Command.Command.setDataList(data, userList, meetingList, userMeeting, consoleOutput);
        }

        //WPF Events
        private void Window_Closed(object sender, EventArgs e)
        {
            Properties.Settings.Default.search_Custom = filterOwnText.Text;
            Properties.Settings.Default.Save();
            data.closeSQL();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.welcome_show == true && Debugger.IsAttached == false)
            {
                new GUI.Welcome().Show();
            }
        }

        //Click Events
        private void consoleCommandRun_Click(object sender, RoutedEventArgs e)
        {
            General.OutPut(consoleCommandInput.Text);
            General.OutPut(Command.Command.commandRun(consoleCommandInput.Text));
            consoleCommandInput.Text = "";
        }

        private void historyReload_Click(object sender, RoutedEventArgs e)
        {
            data.loadHistory(historyList, int.Parse(historyMaxLoad.Text));
        }

        private void meetingAdd_Click(object sender, RoutedEventArgs e)
        {
            DateTime meetingTime = (DateTime)meetingDate.SelectedDate;
            data.addMeeting(General.convertDateTimeToUnixTimestamp(meetingTime));
            data.loadMeetings(meetingList);
        }

        private void meetingRemove_Click(object sender, RoutedEventArgs e)
        {
            if (meetingList.SelectedIndex >= 0)
            {
                if (MessageBox.Show("Bist du dir sicher das du diesen Eintrag löschen möchtest? Auch die Statistik der Teilnehmer wird gelöscht!", "Löschen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    MeetingView m = (MeetingView)meetingList.SelectedItem;
                    data.removeMeeting(int.Parse(m.id));
                    data.loadMeetings(meetingList);
                }
            }
        }

        private void userChangePassword_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                UserView ul = (UserView)userList.SelectedItem;
                new GUI.Password(data, int.Parse(ul.id)).ShowDialog();
            }
        }

        private void userCommentAdd_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0 && userCommentText.Text.Length > 3)
            {
                UserView ul = (UserView)userList.SelectedItem;
                data.addComment(int.Parse(ul.id), UserInfo.LoggedIn.getUID(), userCommentText.Text, General.getUnixTimeNow());
                List<CommentView> comments = data.getComments(int.Parse(ul.id));
                int count = userCommentList.Items.Count;
                for (int i = 0; i < count; i++)
                {
                    userCommentList.Items.RemoveAt(0);
                }
                foreach (CommentView item in comments)
                {
                    userCommentList.Items.Add(new CommentView { by = item.by, comment = item.comment, time = item.time });
                }
                userCommentText.Text = "";
            }
            else if (userCommentText.Text.Length < 3)
            {
                General.OutPut("Please enter a Comment to add!");
            }
        }

        private void userCommentDelete_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0 && userCommentList.SelectedIndex >= 0)
            {
                if (UserInfo.LoggedIn.getAllowDelete() == true)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    CommentView cv = (CommentView)userCommentList.SelectedItem;
                    data.removeComment(int.Parse(ul.id), cv.comment);
                    List<CommentView> comments = data.getComments(int.Parse(ul.id));
                    userCommentList.Items.Clear();
                    foreach (CommentView item in comments)
                    {
                        userCommentList.Items.Add(new CommentView { by = item.by, comment = item.comment, time = item.time });
                    }
                }
                else
                {
                    MessageBox.Show("Du hast nicht die Erlaubnis Kommentare zu löschen!", "Nope!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void userLevelChange_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                if (userLevel.SelectedIndex < 3)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    data.changeUserLevel(int.Parse(ul.id), userLevel.SelectedIndex);
                }
                else if (UserInfo.LoggedIn.getAllowDelete() == true)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    data.changeUserLevel(int.Parse(ul.id), userLevel.SelectedIndex);
                }
                else
                {
                    MessageBox.Show("Du hast nicht die Erlaubnis den Status eines Nutzers auf dieses Level zu setzten! zu löschen!", "Nope!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void userListLoad_Click(object sender, RoutedEventArgs e)
        {
            clanTabs.SelectionChanged -= clanTabs_SelectionChanged;
            userList.SelectionChanged -= userList_SelectionChanged;
            data.loadUserList(userList, (bool)filterBanned.IsChecked, (bool)filterLevel.IsChecked, (bool)filterClanmember.IsChecked, filterLevelSelect.SelectedIndex, (bool)filterOwn.IsChecked, filterOwnText.Text, General.getSortByULCommand((bool)filterSortUserListID.IsChecked, (bool)filterSortUserListNick.IsChecked, (bool)filterSortUserListAge.IsChecked));
            data.loadMeetings(meetingList);
            squadUserViewList = data.loadSquads();
            loadSquads();
            clanTabs.SelectionChanged += clanTabs_SelectionChanged;
            userList.SelectionChanged += userList_SelectionChanged;
        }

        private void userLoadStats_Click(object sender, RoutedEventArgs e)
        {
            int participated = 0;
            int notparticipated = 0;
            int excused = 0;
            int itemcount = userMeeting.Items.Count;
            for (int i = 0; i < itemcount; i++)
            {
                userMeeting.SelectedIndex = i;
                UserMeetingView um = (UserMeetingView)userMeeting.SelectedItem;
                if (General.convertMeetingStatusToInt(um.status) == 0)
                {
                    notparticipated++;
                }
                else if (General.convertMeetingStatusToInt(um.status) == 1)
                {
                    participated++;
                }
                else if (General.convertMeetingStatusToInt(um.status) == 2)
                {
                    excused++;
                }
            }
            userStats.Content = "Teilnahme: Ja: " + participated.ToString("000") + " Nein: " + notparticipated.ToString("000") + " Entschuldigt: " + excused.ToString("000") + " Gesamt: " + (participated + notparticipated + excused).ToString("000");
        }

        private void userMailSend_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                //new GUI.Mail(userMail.Content.ToString().Replace("E-Mail: ", "")).ShowDialog();
                if (userList.SelectedItems.Count == 1)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    Process.Start("mailto:" + ul.eMail + "?subject=[iNv] ");
                }
                else
                {
                    string mailList = string.Empty;
                    List<UserView> ul = new List<UserView>();
                    foreach (UserView item in userList.SelectedItems)
                    {
                        mailList += item.eMail + "; ";
                    }
                    Process.Start("mailto:" + mailList.Remove(mailList.Length - 2) + "?subject=[iNv] ");
                }
            }
        }

        private void userMeetingAdd_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                if (userList.SelectedItems.Count == 1)
                {
                    UserMeetingView um = (UserMeetingView)userMeeting.SelectedItem;
                    UserView ul = (UserView)userList.SelectedItem;
                    data.addUserMeeting(int.Parse(ul.id), int.Parse(userMeetingID.Text), userMeetingStatus.SelectedIndex, userMeetingComment.Text);
                    data.loadMeetingsUser(userMeeting, int.Parse(ul.id), General.getSortByUMCommand((bool)filterSortUserMeetingID.IsChecked, (bool)filterSortUserMeetingComment.IsChecked, (bool)filterSortUserMeetingStatus.IsChecked));
                }
                else
                {
                    foreach (UserView item in userList.SelectedItems)
                    {
                        General.OutPut("Adding User to MeetingList: id:" + item.id + " Nick:" + item.nick);
                        data.addUserMeeting(int.Parse(item.id), int.Parse(userMeetingID.Text), userMeetingStatus.SelectedIndex, userMeetingComment.Text);
                    }
                }
            }
        }

        private void userMeetingChange_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0 && userMeeting.SelectedIndex >= 0)
            {
                UserMeetingView um = (UserMeetingView)userMeeting.SelectedItem;
                UserView ul = (UserView)userList.SelectedItem;
                data.changeMeeting(int.Parse(ul.id), int.Parse(userMeetingID.Text), userMeetingComment.Text, userMeetingStatus.SelectedIndex);
                data.loadMeetingsUser(userMeeting, int.Parse(ul.id), General.getSortByUMCommand((bool)filterSortUserMeetingID.IsChecked, (bool)filterSortUserMeetingComment.IsChecked, (bool)filterSortUserMeetingStatus.IsChecked));
            }
        }

        private void userMeetingRemove_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0 && userMeeting.SelectedIndex >= 0)
            {
                UserMeetingView um = (UserMeetingView)userMeeting.SelectedItem;
                UserView ul = (UserView)userList.SelectedItem;
                data.removeUserMeeting(int.Parse(ul.id), int.Parse(um.meetingid));
                data.loadMeetingsUser(userMeeting, int.Parse(ul.id), General.getSortByUMCommand((bool)filterSortUserMeetingID.IsChecked, (bool)filterSortUserMeetingComment.IsChecked, (bool)filterSortUserMeetingStatus.IsChecked));
            }
        }

        private void userNameChange_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                UserView ul = (UserView)userList.SelectedItem;
                data.setOverwriteName(int.Parse(ul.id), userName.Text);
            }
        }

        private void userOpenAway_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                if (userList.SelectedItems.Count == 1)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    new GUI.Away(data, int.Parse(ul.id), ul.nick).Show();
                }
                else
                {
                    foreach (UserView item in userList.SelectedItems)
                    {
                        new GUI.Away(data, int.Parse(item.id), item.nick).Show();
                    }
                }
            }
        }

        private void userRemove_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                if (UserInfo.LoggedIn.getAllowDelete() == true)
                {
                    if (userList.SelectedItems.Count == 1)
                    {
                        UserView ul = (UserView)userList.SelectedItem;
                        if (MessageBox.Show("Soll der Nutzer \"" + ul.nick + "\" (" + ul.id + ") wirklich gelöscht werden?", "Löschen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            data.removeUser(int.Parse(ul.id));
                            General.OutPut("Reloading Data...");
                            userListLoad_Click(this, new RoutedEventArgs());
                        }
                    }
                    else
                    {
                        List<UserView> ul = new List<UserView>();
                        string uid = "|";
                        foreach (UserView item in userList.SelectedItems)
                        {
                            ul.Add(item);
                            uid += item.id + "|";
                        }
                        if (MessageBox.Show("Sollen wirklich " + ul.Count + " User (" + uid + ") Gelöscht werden?", "Löschen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            foreach (UserView item in ul)
                            {
                                data.removeUser(int.Parse(item.id));
                            }
                            General.OutPut("Reloading Data...");
                            userListLoad_Click(this, new RoutedEventArgs());
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Du hast nicht die Erlaubnis Kommentare zu löschen!", "Nope!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void userShowProfile_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0)
            {
                if (userList.SelectedItems.Count == 1)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    Process.Start("http://invinion.eu/user/?action=user&id=" + ul.id);
                }
                else
                {
                    foreach (UserView item in userList.SelectedItems)
                    {
                        Process.Start("http://invinion.eu/user/?action=user&id=" + item.id);
                    }
                }
            }
        }

        private void userSquadAddToSquad_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0 && userSquadUserList.SelectedIndex >= 0)
            {
                UserView ul = (UserView)userList.SelectedItem;
                SquadView sq = (SquadView)userSquadUserList.SelectedItem;
                if (int.Parse(sq.id) == 1 && UserInfo.LoggedIn.getAllowDelete() == false)
                {
                    MessageBox.Show("Du has keine Rechte für diese Aufgabe!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    data.removeUserSqads(int.Parse(ul.id), int.Parse(sq.id));
                    loadSquads();
                    data.getUserSqads(userSquadList, userSquadUserList, int.Parse(ul.id));
                }
            }
        }

        private void userSquadAddToUser_Click(object sender, RoutedEventArgs e)
        {
            if (userList.SelectedIndex >= 0 && userSquadList.SelectedIndex >= 0)
            {
                UserView ul = (UserView)userList.SelectedItem;
                SquadView sq = (SquadView)userSquadList.SelectedItem;
                if (int.Parse(sq.id) == 1 && UserInfo.LoggedIn.getAllowDelete() == false)
                {
                    MessageBox.Show("Du has keine Rechte für diese Aufgabe!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    data.addUserSqads(int.Parse(ul.id), int.Parse(sq.id));
                    loadSquads();
                    data.getUserSqads(userSquadList, userSquadUserList, int.Parse(ul.id));
                }
            }
        }

        private void userSquadPosiChange_Click(object sender, RoutedEventArgs e)
        {
            if (userSquadUserList.SelectedIndex >= 0 && userList.SelectedIndex >= 0)
            {
                if (UserInfo.LoggedIn.getAllowDelete() == false && userSquadPosi.SelectedIndex >= 5)
                {
                    MessageBox.Show("Du hast keine Rechte um einen Nutzer dieses Leve zu geben!", "Fehlende Rechte!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    SquadView sv = (SquadView)userSquadUserList.SelectedItem;
                    UserView ul = (UserView)userList.SelectedItem;
                    int posi;
                    switch (userSquadPosi.SelectedIndex)
                    {
                        case 0:
                            posi = 0;
                            break;

                        case 1:
                            posi = 6;
                            break;

                        case 2:
                            posi = 4;
                            break;

                        case 3:
                            posi = 5;
                            break;

                        case 4:
                            posi = 2;
                            break;

                        case 5:
                            posi = 1;
                            break;

                        default:
                            General.OutPut("Please Check your SQL for anny changes!");
                            posi = -1;
                            break;
                    }
                    if (posi != -1)
                    {
                        data.changeSquadPosi(int.Parse(ul.id), int.Parse(sv.id), posi);
                    }
                }
            }
        }

        //Changed Event
        private void clanTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (clanTabs.SelectedIndex == clanTabs.Items.Count - 3)
            {
                clanTabs.SelectedIndex = userListAndFilterLastTab;

                new GUI.Settings(data).ShowDialog();
            }
            else if (clanTabs.SelectedIndex == clanTabs.Items.Count - 2)
            {
                clanTabs.SelectedIndex = userListAndFilterLastTab;

                userListLoad_Click(this, new RoutedEventArgs());
            }
            else if (clanTabs.SelectedIndex == clanTabs.Items.Count - 1)
            {
                clanTabs.SelectedIndex = userListAndFilterLastTab;

                new GUI.IRCChat(UserInfo.LoggedIn.getUName()).Show();
            }
            else
            {
                userListAndFilterLastTab = clanTabs.SelectedIndex;
            }
        }

        private void filterLevel_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (filterLevel.IsChecked == true && filterLevel.IsEnabled == true)
            {
                filterLevelSelect.IsEnabled = true;
                filterClanmember.IsEnabled = false;
            }
            else
            {
                filterLevelSelect.IsEnabled = false;
                filterClanmember.IsEnabled = true;
            }
        }

        private void filterOwn_CheckedChecked(object sender, RoutedEventArgs e)
        {
            if (filterOwn.IsChecked == true)
            {
                filterOwnText.IsEnabled = true;
                filterLevel.IsEnabled = false;
                filterBanned.IsEnabled = false;
                filterClanmember.IsEnabled = false;
            }
            else
            {
                filterOwnText.IsEnabled = false;
                filterLevel.IsEnabled = true;
                filterBanned.IsEnabled = true;
                filterClanmember.IsEnabled = true;
            }
            filterLevel_CheckedChanged(this, new RoutedEventArgs());
        }

        private void userList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearUserUI();
            if (userList.SelectedIndex >= 0)
            {
                if (userList.SelectedItems.Count == 1)
                {
                    UserView ul = (UserView)userList.SelectedItem;
                    List<CommentView> comments = data.getComments(int.Parse(ul.id));

                    userDbID.Content = "DB ID: " + ul.id;
                    userNick.Content = "Nick: " + ul.nick;
                    userLastlogin.Content = "Letzter Login: " + ul.lastlogin;
                    string OverwriteName = data.getOverwriteName(int.Parse(ul.id));
                    if (OverwriteName.Length > 2) //Namen Anzeigen
                    {
                        userName.Text = OverwriteName;
                    }
                    else
                    {
                        userName.Text = ul.name;
                    } //Namen Anzeigen Ende
                    userAge.Content = "Alter: " + ul.age;
                    userMail.Content = "E-Mail: " + data.getUserMail(int.Parse(ul.id));

                    //Bild Laden
                    profilePicjpg.Source = new BitmapImage(new Uri("http://invinion.eu/inc/images/uploads/useravatare/" + ul.id + ".jpg", UriKind.Absolute));
                    profilePicpng.Source = new BitmapImage(new Uri("http://invinion.eu/inc/images/uploads/useravatare/" + ul.id + ".png", UriKind.Absolute));
                    // Bild Laden Ende

                    userLevel.SelectedIndex = data.getUserLevel(int.Parse(ul.id));

                    if (comments != null)
                    {
                        foreach (CommentView item in comments)
                        {
                            userCommentList.Items.Add(new CommentView { by = item.by, comment = item.comment, time = item.time });
                        } //Kommentare Laden Ende
                    }

                    data.loadMeetingsUser(userMeeting, int.Parse(ul.id), General.getSortByUMCommand((bool)filterSortUserMeetingID.IsChecked, (bool)filterSortUserMeetingComment.IsChecked, (bool)filterSortUserMeetingStatus.IsChecked));

                    userLoadStats_Click(this, new RoutedEventArgs());
                    loadSquads();

                    data.getUserSqads(userSquadList, userSquadUserList, int.Parse(ul.id));
                }
                else
                {
                    loadSquads();
                    setMultiSelectUI(false);
                    userDbID.Content = "|";
                    List<UserView> ul = new List<UserView>();
                    int count = 0;
                    foreach (UserView item in userList.SelectedItems)
                    {
                        userDbID.Content += item.id.ToString() + "|";
                        ul.Add(item);
                        count++;
                    }
                    userDbID.Content = "DB ID (" + count.ToString("000") + "): " + userDbID.Content;
                }
            }
        }

        private void userListSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (userList.Items.Count > 0 || userListSearch.Text.Length > 0)
            {
                userList.SelectedItems.Clear();
                foreach (UserView item in userList.Items)
                {
                    if (containsComparison(item.id.ToString(), userListSearch.Text, StringComparison.OrdinalIgnoreCase) || containsComparison(item.name, userListSearch.Text, StringComparison.OrdinalIgnoreCase) || containsComparison(item.nick, userListSearch.Text, StringComparison.OrdinalIgnoreCase) || containsComparison(item.lastlogin, userListSearch.Text, StringComparison.OrdinalIgnoreCase) || containsComparison(item.eMail, userListSearch.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        userList.SelectedItems.Add(item);
                    }
                    userListSearch.Focus();
                }
            }
        }

        private void userMeeting_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (userMeeting.SelectedIndex >= 0)
            {
                UserMeetingView um = (UserMeetingView)userMeeting.SelectedItem;
                userMeetingComment.Text = um.comment;
                userMeetingID.Text = um.meetingid;
                userMeetingStatus.SelectedIndex = General.convertMeetingStatusToInt(um.status);
            }
        }

        private void userSquadUserList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (userSquadUserList.SelectedIndex >= 0 && userList.SelectedIndex >= 0)
            {
                SquadView sv = (SquadView)userSquadUserList.SelectedItem;
                UserView ul = (UserView)userList.SelectedItem;
                int posi = data.getUserSquadPosi(int.Parse(ul.id), int.Parse(sv.id));
                switch (posi)
                {
                    case 0:
                        userSquadPosi.SelectedIndex = 0;
                        break;

                    case 1:
                        userSquadPosi.SelectedIndex = 5;
                        break;

                    case 2:
                        userSquadPosi.SelectedIndex = 4;
                        break;

                    case 4:
                        userSquadPosi.SelectedIndex = 2;
                        break;

                    case 5:
                        userSquadPosi.SelectedIndex = 3;
                        break;

                    case 6:
                        userSquadPosi.SelectedIndex = 1;
                        break;

                    default:
                        General.OutPut("Please Check your SQL for anny changes!");
                        break;
                }
            }
        }

        //Input Event
        private void CheckIfInt_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            string[] zeroTonine = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            if (zeroTonine.Contains(e.Text) == true)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}