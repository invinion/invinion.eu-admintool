﻿namespace INVINION.UserInfo
{
    internal static class LoggedIn
    {
        private static int UID;
        private static string UName;
        private static bool AllowDelete;

        //Get Methoden
        public static bool getAllowDelete()
        {
            return AllowDelete;
        }

        public static string getUName()
        {
            return UName;
        }

        public static int getUID()
        {
            return UID;
        }

        //SetMethoden
        public static void setAllowDelete(bool allow)
        {
            AllowDelete = allow;
        }

        public static void setUName(string name)
        {
            UName = name;
        }

        public static void setUID(int id)
        {
            UID = id;
        }
    }
}