﻿using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Windows;
using System.Xml;

namespace INVINION.Version
{
    internal class Updater
    {
        public Updater()
        {
            new Thread(checkUpdates).Start();
        }

        private void checkUpdates()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("http://invinion.eu/admintool/UpdateList.xml");

                XmlNode version = doc.SelectSingleNode("/AdminTool/Version");
                XmlNode updater = doc.SelectSingleNode("/AdminTool/Updater");

                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    General.OutPut("Updater: Aktuelle Programm-Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + " Aktuelle WebVersion: " + version.InnerText);
                }));

                if (System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() != version.InnerText && Debugger.IsAttached == false)
                {
                    WebClient updaterLoad = new WebClient();
                    updaterLoad.DownloadFile(updater.Attributes["download"].InnerText, "Updater.exe");
                    if (MessageBox.Show("Eine Neue Version des Programmes wurde gefunden soll jetzt geupdatet werden?", "Neue Version", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Process.Start("Updater.exe");
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            Application.Current.Shutdown();
                        }));
                    }
                }
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    General.OutPut("Updater: " + ex.ToString());
                }));
            }
        }
    }
}