﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Jan18101997.Utility
{
    internal static class Utility
    {
        /// <summary>
        /// Compares two VersionStrings
        /// </summary>
        /// <param name="currentVersion">Version String of the CurrentVersion</param>
        /// <param name="newVersion">Version String of the new Version</param>
        /// <returns>retuns 0 if versions are equal retuns 1 if versions is newer retuns -1 if versions is older</returns>
        public static int CompareVersions(string currentVersion, string newVersion)
        {
            int[] newV = ConvertVersionStringToIntArr(newVersion);
            int[] currentV = ConvertVersionStringToIntArr(currentVersion);

            for (int i = 0; i < newV.Length; i++)
            {
                if (newV[i] > currentV[i])
                {
                    return 1;
                }
                if (newV[i] < currentV[i])
                {
                    return -1;
                }
            }

            return 0;
        }

        /// <summary>
        /// Converts a Version string to a int arr with seperat parts
        /// </summary>
        /// <param name="VersionString"></param>
        /// <returns>int Arr</returns>
        public static int[] ConvertVersionStringToIntArr(string VersionString)
        {
            if (IsVersionString(VersionString) == false)
            {
                throw new ArgumentException("Please enter a valid version number!");
            }

            string[] VersionSections = VersionString.Split('.');
            int[] VersionSectionsInt = new int[VersionSections.Length];
            for (int i = 0; i < VersionSections.Length; i++)
            {
                VersionSectionsInt[i] = int.Parse(VersionSections[i]);
            }

            return VersionSectionsInt;
        }

        /// <summary>
        /// Checks if the String is a valid version string
        /// </summary>
        /// <param name="versionString">Version string to check</param>
        /// <returns>if the sting is a valid version string</returns>
        public static bool IsVersionString(string versionString)
        {
            return Regex.IsMatch(versionString, @"\d{1,9}\.\d{1,9}\.\d{1,9}\.\d{1,9}");
        }

        /// <summary>
        /// Converts a unix time to DataTime
        /// </summary>
        /// <param name="unixTimeStamp">unix time stamp</param>
        /// <returns>DataTime</returns>
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        /// Converts a Strig to a MD5 Hash
        /// </summary>
        /// <param name="value">string to convert</param>
        /// <returns>Hash-Code</returns>
        public static string GetMD5Hash(string value)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                // ANSI (varchar)
                var valueBytes = Encoding.Default.GetBytes(value);
                var md5HashBytes = md5.ComputeHash(valueBytes);
                var builder = new StringBuilder(md5HashBytes.Length * 2);
                foreach (var md5Byte in md5HashBytes)
                    builder.Append(md5Byte.ToString("X2"));
                return builder.ToString();
            }
        }

        /// <summary>
        /// Retourn a string that contiains new Lienes
        /// </summary>
        /// <param name="times">How many new Lines</param>
        /// <returns>The NewLine String</returns>
        public static string NewLine(int times)
        {
            string ret = string.Empty;
            for (int i = 0; i < times; i++)
            {
                ret += Environment.NewLine;
            }
            return ret;
        }
    }
}