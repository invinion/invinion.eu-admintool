﻿using Jan18101997.Utility;
using System;

namespace INVINION
{
    internal static class General
    {
        private static System.Windows.Controls.TextBox outputBox;
        private static System.Windows.Controls.ScrollViewer scrollview;

        //Set Methoden
        public static void setOutputBox(System.Windows.Controls.TextBox box)
        {
            outputBox = box;
        }

        public static void setScrollViewer(System.Windows.Controls.ScrollViewer view)
        {
            scrollview = view;
        }

        //Get Methoden
        public static double getAgeByBDay(double bDay)
        {
            DateTime b = Utility.UnixTimeStampToDateTime(bDay);
            DateTime now = Utility.UnixTimeStampToDateTime(getUnixTimeNow());

            return Math.Truncate((now - b).TotalDays / 365);
        }

        public static string getSortByULCommand(bool id, bool nick, bool age)
        {
            if (id == true)
            {
                return "ORDER BY id ASC";
            }
            else if (nick == true)
            {
                return "ORDER BY nick ASC";
            }
            else if (age == true)
            {
                return "ORDER BY bday DESC";
            }
            else
            {
                General.OutPut("Unable to Generate SORT BY Command");
                return "";
            }
        }

        public static string getSortByUMCommand(bool id, bool comment, bool status)
        {
            if (id == true)
            {
                return "ORDER BY meetingid ASC";
            }
            else if (comment == true)
            {
                return "ORDER BY comment ASC";
            }
            else if (status == true)
            {
                return "ORDER BY status ASC";
            }
            else
            {
                General.OutPut("Unable to Generate SORT BY Command");
                return "";
            }
        }

        public static double getUnixTimeNow()
        {
            DateTime nx = new DateTime(1970, 1, 1); // UNIX epoch date
            TimeSpan ts = DateTime.UtcNow - nx; // UtcNow, because timestamp is in GMT

            return Math.Round(ts.TotalSeconds);
        }

        //Converter Methoden
        public static string convertBoolToYesNo(bool value)
        {
            switch (value)
            {
                case false:
                    return "Nein";

                case true:
                    return "JA!";

                default:
                    throw new IndexOutOfRangeException();
            }
        }

        public static int convertBoolToInt(bool b)
        {
            switch (b)
            {
                case true:
                    return 1;

                case false:
                    return 0;

                default: throw new IndexOutOfRangeException();
            }
        }

        public static double convertDateTimeToUnixTimestamp(DateTime date)
        {
            double unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
            unixTimestamp /= TimeSpan.TicksPerSecond;
            return unixTimestamp;
        }

        public static string convertIntToYesNo(int i)
        {
            switch (i)
            {
                case 0:
                    return "Nein";

                case 1:
                    return "Ja";

                default:
                    throw new IndexOutOfRangeException();
            }
        }

        public static int convertMeetingStatusToInt(string status)
        {
            switch (status)
            {
                case "Nein":
                    return 0;

                case "Ja":
                    return 1;

                case "Entschuldigt":
                    return 2;

                default: throw new OutOfMemoryException();
            }
        }

        public static string convertMeetingStatusToString(int status)
        {
            switch (status)
            {
                case 0:
                    return "Nein";

                case 1:
                    return "Ja";

                case 2:
                    return "Entschuldigt";

                default: throw new OutOfMemoryException();
            }
        }

        public static string convertTimeStampToString(double timestamp, bool time)
        {
            DateTime date = Utility.UnixTimeStampToDateTime(timestamp);
            if (time == true)
            {
                return date.Day.ToString("00") + "." + date.Month.ToString("00") + "." + date.Year.ToString("00") + " " + date.Hour.ToString("00") + ":" + date.Minute.ToString("00");
            }
            return date.Day.ToString("00") + "." + date.Month.ToString("00") + "." + date.Year.ToString("00");
        }

        public static bool convertYesNoToBool(string yn)
        {
            switch (yn)
            {
                case "Nein":
                    return false;

                case "Ja":
                    return true;

                default:
                    throw new IndexOutOfRangeException();
            }
        }

        //Ausführende Methoden
        public static void OutPut(string message)
        {
            message = "(" + DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00") + "): " + message + Environment.NewLine;
            System.Diagnostics.Debug.Write(message);
            outputBox.Text += message;
            scrollview.Focus();
            scrollview.ScrollToBottom();
        }
    }
}