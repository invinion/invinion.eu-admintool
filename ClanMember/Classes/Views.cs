﻿namespace INVINION.Views
{
    public class AwayView
    {
        public string id { get; set; }

        public string titel { get; set; }

        public string start { get; set; }

        public string end { get; set; }
    }

    public class CommentView
    {
        public string comment { get; set; }

        public string time { get; set; }

        public string by { get; set; }
    }

    public class HistoryView
    {
        public string logID { get; set; }

        public string comment { get; set; }

        public string userID { get; set; }

        public string time { get; set; }
    }

    public class MeetingView
    {
        public string id { get; set; }

        public string time { get; set; }

        public string participated { get; set; }

        public string notparticipated { get; set; }

        public string excused { get; set; }

        public string total { get; set; }
    }

    public class RightsView
    {
        public string id { get; set; }

        public string allowdelete { get; set; }
    }

    public class SquadView
    {
        public string id { get; set; }

        public string name { get; set; }
    }

    public class UserMeetingView
    {
        public string meetingid { get; set; }

        public string time { get; set; }

        public string comment { get; set; }

        public string status { get; set; }
    }

    public class UserView
    {
        public string id { get; set; }

        public string nick { get; set; }

        public string name { get; set; }

        public string lastlogin { get; set; }

        public string age { get; set; }

        public string eMail { get; set; }
    }
}