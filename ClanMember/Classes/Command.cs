﻿using MySql.Data.MySqlClient;
using System;

namespace INVINION.Command
{
    internal static class Command
    {
        private static MySqlConnector datal;
        private static System.Windows.Controls.ListView UserList;
        private static System.Windows.Controls.ListView MeetingList;
        private static System.Windows.Controls.ListView MeetingUserList;
        private static System.Windows.Controls.TextBox ConsoleBox;

        //Set Methoden
        public static void setDataList(MySqlConnector data, System.Windows.Controls.ListView user, System.Windows.Controls.ListView meeting, System.Windows.Controls.ListView meetinguser, System.Windows.Controls.TextBox con)
        {
            datal = data;
            UserList = user;
            MeetingList = meeting;
            MeetingUserList = meetinguser;
            ConsoleBox = con;
        }

        //Get Methoden
        private static string getCommand(string command)
        {
            if ((command.Split(' ')).Length > 0)
            {
                return (command.Split(' '))[0];
            }
            else
            {
                return command.Replace(" ", "");
            }
        }

        //Ausführende Methoden
        public static string commandRun(string command)
        {
            string ret = "";
            switch (getCommand(command))
            {
                case "help":
                    return "help - Shows this help" + Environment.NewLine + "sqladmin - Run SQL Commands at admin_tool DataBase" + Environment.NewLine + "sqlclan - Run SQL Commands at clan DataBase" + Environment.NewLine + "clear - clears the GUI and deload all lists";

                case "sqladmin":
                    try
                    {
                        MySqlConnection connection_admintool = datal.getConnectionAdmintool();
                        MySqlCommand command_admintool = datal.getCommandAdmintool();
                        command_admintool.CommandText = command.Replace(getCommand(command), "");
                        MySqlDataReader Reader_admintool;
                        Reader_admintool = command_admintool.ExecuteReader();
                        ret += "MySQL@amdin tool: ";
                        while (Reader_admintool.Read())
                        {
                            for (int i = 0; i < Reader_admintool.FieldCount; i++)
                            {
                                ret += Reader_admintool.GetValue(i).ToString() + " , ";
                            }
                            ret += Environment.NewLine;
                        }
                        Reader_admintool.Close();
                    }
                    catch (Exception ex)
                    {
                        ret = ex.ToString();
                    }
                    return ret;

                case "sqlclan":
                    try
                    {
                        MySqlConnection connection_clan = datal.getConnectionClan();
                        MySqlCommand command_clan = datal.getCommandClan();
                        command_clan.CommandText = command.Replace(getCommand(command), "");
                        MySqlDataReader Reader_clan;
                        Reader_clan = command_clan.ExecuteReader();
                        ret += "MySQL@clan: ";
                        while (Reader_clan.Read())
                        {
                            for (int i = 0; i < Reader_clan.FieldCount; i++)
                            {
                                ret += Reader_clan.GetValue(i).ToString() + " , ";
                            }
                            ret += Environment.NewLine;
                        }
                        Reader_clan.Close();
                    }
                    catch (Exception ex)
                    {
                        ret = ex.ToString();
                    }
                    return ret;

                case "clear":
                    int itemcount = UserList.Items.Count;
                    for (int i = 0; i < itemcount; i++)
                    {
                        UserList.Items.RemoveAt(0);
                    }
                    itemcount = MeetingList.Items.Count;
                    for (int i = 0; i < itemcount; i++)
                    {
                        MeetingList.Items.RemoveAt(0);
                    }
                    itemcount = MeetingUserList.Items.Count;
                    for (int i = 0; i < itemcount; i++)
                    {
                        MeetingUserList.Items.RemoveAt(0);
                    }
                    ConsoleBox.Text = "";
                    return "GUI Cleared";

                default:
                    return "Command '" + getCommand(command) + "' not found use help to get all avalible Commands.";
            }
        }
    }
}