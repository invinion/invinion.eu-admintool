﻿using INVINION.Views;
using Jan18101997.Time;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace INVINION
{
    public class MySqlConnector
    {
        public MySqlConnector()
        {
            swatch = new StopWatch();
            openSQL();
        }

        private MySqlConnection connection_clan;
        private MySqlConnection connection_admintool;
        private MySqlCommand command_clan;
        private MySqlCommand command_admintool;
        private StopWatch swatch;

        //get MySQL Methoden
        public MySqlConnection getConnectionClan()
        {
            return connection_clan;
        }

        public MySqlConnection getConnectionAdmintool()
        {
            return connection_admintool;
        }

        public MySqlCommand getCommandClan()
        {
            return command_clan;
        }

        public MySqlCommand getCommandAdmintool()
        {
            return command_admintool;
        }

        //Void Methoden
        public void openSQL()
        {
            try
            {
                connection_clan = new MySqlConnection(PrivateData.MySQLLogin.getclanMySqlLogin());
                connection_admintool = new MySqlConnection(PrivateData.MySQLLogin.getadmintoolMySqlLogin());
                command_clan = connection_clan.CreateCommand();
                connection_clan.Open();
                command_admintool = connection_admintool.CreateCommand();
                connection_admintool.Open();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                System.Windows.MessageBox.Show("Unable to Connect to MySQL! The Programm is not able to start!", "No Connection", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                Environment.Exit(0);
            }
        }

        public void closeSQL()
        {
            connection_admintool.Close();
            connection_clan.Close();
        }

        //LoadData Methoden
        public void loadUserList(System.Windows.Controls.ListView view, bool banned, bool lvl, bool justuser, int lvllvl, bool own, string ownText, string SortCommand)
        {
            swatch.Start(0.0);
            General.OutPut("Starting DataLoading...");
            try
            {
                view.Items.Clear();

                string Where = string.Empty;
                if(own == false)
                {
                    if (banned == false && lvl == false && justuser == false)
                    {
                        Where = string.Empty;
                    }
                    else if(banned == true && lvl == false && justuser == false)
                    {
                        Where = "WHERE (NOT (clanusers.banned = 1))";
                    }
                    else if (banned == false && lvl == true && justuser == false)
                    {
                        Where = "WHERE clanusers.level=" + lvllvl.ToString();
                    }
                    else if (banned == false && lvl == false && justuser == true)
                    {
                        Where = "WHERE clanusers.level>2";
                    }
                    else if (banned == true && lvl == false && justuser == true)
                    {
                        Where = "WHERE (clanusers.level>2) AND (NOT (clanusers.banned = 1))";
                    }
                    else if (banned == true && lvl == true)
                    {
                        Where = "WHERE (clanusers.level=" + lvllvl.ToString() + ") AND (NOT (clanusers.banned = 1))";
                    }
                }

                if (own == true)
                {
                    command_clan.CommandText = "SELECT clanusers.id, clanusers.nick, clanusers.rlname, clanusers.bday, clanuserstats.lastvisit, clanusers.email FROM clanusers INNER JOIN clanuserstats ON clanusers.id = clanuserstats.`user` " + ownText + " " + SortCommand;
                }
                else
                {
                    command_clan.CommandText = "SELECT clanusers.id, clanusers.nick, clanusers.rlname, clanusers.bday, clanuserstats.lastvisit, clanusers.email FROM clanusers INNER JOIN clanuserstats ON clanusers.id = clanuserstats.`user` " + Where + " " + SortCommand;
                }
                General.OutPut("Running with \"" + command_clan.CommandText + "\"");

                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    string Age = "N/A";
                    if (int.Parse(Reader_clan.GetValue(3).ToString()) != 0)
                    {
                        Age = General.getAgeByBDay(double.Parse(Reader_clan.GetValue(3).ToString())).ToString();
                    }

                    view.Items.Add(new UserView { id = Reader_clan.GetValue(0).ToString(), nick = Reader_clan.GetValue(1).ToString(), name = Reader_clan.GetValue(2).ToString(), age = Age, lastlogin = General.convertTimeStampToString(double.Parse(Reader_clan.GetValue(4).ToString()), true), eMail = Reader_clan.GetValue(5).ToString() });
                    General.OutPut("Adding Users to List UID: " + Reader_clan.GetValue(0).ToString());
                }
                Reader_clan.Close();
                for (int i = 0; i < view.Items.Count; i++)
                {
                    view.SelectedIndex = i;
                    UserView ul = (UserView)view.SelectedItem;
                    if (ul.name.Length <= 2)
                    {
                        ul.name = getOverwriteName(int.Parse(ul.id));
                    }
                }
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
        }

        public void loadMeetings(System.Windows.Controls.ListView view)
        {
            swatch.Start(0.0);
            General.OutPut("Starting DataLoading from Meetings...");
            try
            {
                view.Items.Clear();

                command_admintool.CommandText = "SELECT meetingid, time FROM tool_meeting";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    view.Items.Add(new MeetingView { id = Reader_admintool.GetValue(0).ToString(), time = Reader_admintool.GetValue(1).ToString() });
                }
                Reader_admintool.Close();

                for (int i = 0; i < view.Items.Count; i++)
                {
                    int participated = 0;
                    int notparticipated = 0;
                    int excused = 0;
                    int total = 0;
                    view.SelectedIndex = i;
                    MeetingView ms = (MeetingView)view.SelectedItem;
                    command_admintool.CommandText = "SELECT status FROM tool_meetings_user WHERE meetingid=" + ms.id;
                    General.OutPut("Loading Meeting Stats for id: " + ms.id);
                    Reader_admintool = command_admintool.ExecuteReader();
                    while (Reader_admintool.Read())
                    {
                        if (int.Parse(Reader_admintool.GetValue(0).ToString()) == 0)
                        {
                            notparticipated++;
                        }
                        else if (int.Parse(Reader_admintool.GetValue(0).ToString()) == 1)
                        {
                            participated++;
                        }
                        else if (int.Parse(Reader_admintool.GetValue(0).ToString()) == 2)
                        {
                            excused++;
                        }
                        total++;
                    }
                    ms.participated = participated.ToString();
                    ms.notparticipated = notparticipated.ToString();
                    ms.excused = excused.ToString();
                    ms.total = total.ToString();
                    ms.time = General.convertTimeStampToString(double.Parse(ms.time), false);
                    Reader_admintool.Close();
                }
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
        }

        public void loadMeetingsUser(System.Windows.Controls.ListView view, int userID, string SortCommand)
        {
            swatch.Start(0.0);
            General.OutPut("Starting DataLoading from Meetings...");
            try
            {
                view.Items.Clear();
                command_admintool.CommandText = "SELECT comment, status, meetingid FROM tool_meetings_user WHERE uid=" + userID.ToString() + " " + SortCommand;
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    General.OutPut("Loading Meetins for UID: " + userID.ToString());
                    view.Items.Add(new UserMeetingView { comment = Reader_admintool.GetValue(0).ToString(), status = General.convertMeetingStatusToString(int.Parse(Reader_admintool.GetValue(1).ToString())), meetingid = Reader_admintool.GetValue(2).ToString() });
                }
                Reader_admintool.Close();
                for (int i = 0; i < view.Items.Count; i++)
                {
                    view.SelectedIndex = i;
                    UserMeetingView um = (UserMeetingView)view.SelectedItem;
                    command_admintool.CommandText = "SELECT time FROM tool_meeting WHERE meetingid=" + um.meetingid;
                    General.OutPut("Loading Information for MeetingID: " + um.meetingid);
                    Reader_admintool = command_admintool.ExecuteReader();
                    while (Reader_admintool.Read())
                    {
                        um.time = General.convertTimeStampToString(int.Parse(Reader_admintool.GetValue(0).ToString()), false);
                    }
                    Reader_admintool.Close();
                }
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
        }

        public void loadHistory(System.Windows.Controls.ListView view, int Limit)
        {
            swatch.Start(0.0);
            General.OutPut("Starting HistoryLoading...");
            try
            {
                view.Items.Clear();

                if (Limit == 0)
                {
                    command_admintool.CommandText = "SELECT logid, comment, uid, time FROM tool_log WHERE 1 ORDER BY time DESC";
                }
                else
                {
                    command_admintool.CommandText = "SELECT logid, comment, uid, time FROM tool_log WHERE 1 ORDER BY time DESC LIMIT " + Limit.ToString();
                }
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    view.Items.Add(new HistoryView { logID = Reader_admintool.GetValue(0).ToString(), comment = Reader_admintool.GetValue(1).ToString(), userID = Reader_admintool.GetValue(2).ToString(), time = General.convertTimeStampToString(double.Parse(Reader_admintool.GetValue(3).ToString()), true) });
                    General.OutPut("Loading to History " + Reader_admintool.GetValue(0).ToString() + " " + Reader_admintool.GetValue(1).ToString() + " " + Reader_admintool.GetValue(2).ToString() + " " + General.convertTimeStampToString(double.Parse(Reader_admintool.GetValue(3).ToString()), true));
                }
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
        }

        public List<SquadView> loadSquads()
        {
            swatch.Start(0.0);
            General.OutPut("Starting DataLoading for Squads");
            List<SquadView> view = new List<SquadView>();
            try
            {
                command_clan.CommandText = "SELECT id, name FROM clansquads ORDER BY name";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    view.Add(new SquadView { id = Reader_clan.GetValue(0).ToString(), name = Reader_clan.GetValue(1).ToString() });
                }
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            return view;
        }

        public void loadAway(System.Windows.Controls.ListView view, int UID)
        {
            swatch.Start(0.0);
            General.OutPut("Starting DataLoading from Away...");
            try
            {
                command_clan.CommandText = "SELECT id, titel, start, end FROM clanaway WHERE (userid = " + UID + ") ORDER BY start";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    view.Items.Add(new AwayView { id = Reader_clan.GetValue(0).ToString(), titel = Reader_clan.GetValue(1).ToString(), start = General.convertTimeStampToString(double.Parse(Reader_clan.GetValue(2).ToString()), false), end = General.convertTimeStampToString(double.Parse(Reader_clan.GetValue(3).ToString()), false) });
                }
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
        }

        public void loadRights(System.Windows.Controls.ListView view)
        {
            swatch.Start(0.0);
            General.OutPut("Starting DataLoading from Rigts...");
            view.Items.Clear();
            try
            {
                command_admintool.CommandText = "SELECT id, admin FROM tool_login ORDER BY id";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    view.Items.Add(new RightsView { id = Reader_admintool.GetValue(0).ToString(), allowdelete = General.convertIntToYesNo(int.Parse(Reader_admintool.GetValue(1).ToString())) });
                }
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
        }

        //Add Methoden
        public void addMeeting(double timestamp)
        {
            swatch.Start(0.0);
            try
            {
                command_admintool.CommandText = "INSERT INTO tool_meeting (time) VALUES (" + timestamp + ")";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Read();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Added Meeting at " + timestamp.ToString());
            addHistory("Added Meeting at " + timestamp.ToString());
        }

        public void addComment(int id, int byID, string comment, double timestamp)
        {
            swatch.Start(0.0);
            try
            {
                command_admintool.CommandText = "INSERT INTO tool_comment(UID, byUID, comment, time) VALUES ('" + id.ToString() + "','" + byID.ToString() + "','" + comment + "','" + timestamp + "')";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Added Comment '" + comment + "' to ID: " + id.ToString() + " in " + swatch.Time.ToString() + "sek");
            addHistory("Added Comment to " + id.ToString() + " with '" + comment + "' at " + timestamp.ToString());
        }

        public void addHistory(string Comment)
        {
            swatch.Start(0.0);
            try
            {
                command_admintool.CommandText = "INSERT INTO tool_log(comment, uid, time) VALUES ('" + Comment + "', " + UserInfo.LoggedIn.getUID().ToString() + ", " + General.getUnixTimeNow() + ")";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut("HistoryError -> " + ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Added to History in " + swatch.Time.ToString() + "sek.");
        }

        public void addRights(int UID, bool admin)
        {
            swatch.Start(0.0);
            General.OutPut("Adding Rights...");
            try
            {
                command_admintool.CommandText = "INSERT INTO tool_login (id, admin) VALUES (" + UID.ToString() + "," + General.convertBoolToInt(admin).ToString() + ")";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Added Rights for " + UID + " Admin " + admin);
        }

        public void addUserMeeting(int UID, int id, int status, string comment)
        {
            swatch.Start(0.0);
            General.OutPut("Starting...");
            try
            {
                bool meetingExists = false;
                command_admintool.CommandText = "SELECT meetingid FROM tool_meeting WHERE meetingid=" + id.ToString();
                General.OutPut("Checking if Meeting Exists for id: " + id.ToString());
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    meetingExists = true;
                }
                Reader_admintool.Close();
                if (meetingExists == true)
                {
                    command_admintool.CommandText = "INSERT INTO tool_meetings_user(uid, comment, meetingid, status) VALUES (" + UID.ToString() + ",'" + comment + "'," + id.ToString() + "," + status.ToString() + ")";
                    General.OutPut("Adding Meeting id: " + id.ToString() + " to UID: " + UID.ToString());
                    Reader_admintool = command_admintool.ExecuteReader();
                    while (Reader_admintool.Read())
                    {
                    }
                    Reader_admintool.Close();
                }
                else
                {
                    General.OutPut("Unable to find Meeting by id: " + id.ToString());
                }
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Added Meeting to User " + UID.ToString() + " for " + id.ToString() + " with status " + status.ToString() + " and Comment " + comment);
        }

        public void addUserSqads(int UID, int id)
        {
            try
            {
                command_clan.CommandText = "INSERT INTO clansquaduser(user, squad) VALUES (" + UID.ToString() + "," + id.ToString() + ")";
                General.OutPut(command_clan.CommandText);
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read()) { }
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            addHistory("Added Squad " + id.ToString() + " from user " + UID.ToString());
        }

        //Set Methoden
        public void setOverwriteName(int UID, string name)
        {
            if (name.Length > 2)
            {
                swatch.Start(0.0);
                try
                {
                    command_admintool.CommandText = "DELETE FROM tool_overwrite WHERE id=" + UID.ToString();
                    MySqlDataReader Reader_admintool;
                    Reader_admintool = command_admintool.ExecuteReader();
                    Reader_admintool.Close();

                    command_admintool.CommandText = "INSERT INTO `tool_overwrite`(`id`, `name`) VALUES ('" + UID.ToString() + "','" + name + "')";
                    Reader_admintool = command_admintool.ExecuteReader();
                    Reader_admintool.Close();

                    addHistory("Changed OverwriteName for " + UID.ToString() + " to " + name);
                }
                catch (Exception ex)
                {
                    General.OutPut(ex.ToString());
                }
                swatch.Stop();
                General.OutPut("Changed OverwriteName for " + UID.ToString() + " to " + name + " in " + swatch.Time.ToString() + "sek.");
            }
        }

        //Remove Methoden
        public void removeMeeting(int meetingID)
        {
            swatch.Start(0.0);
            General.OutPut("Starting...");
            try
            {
                command_admintool.CommandText = "DELETE FROM `tool_meeting` WHERE meetingid=" + meetingID.ToString();
                General.OutPut("Deleting Meeting from meetingList for id: " + meetingID.ToString());
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();

                command_admintool.CommandText = "DELETE FROM `tool_meetings_user` WHERE meetingid=" + meetingID.ToString();
                General.OutPut("Deleting Meeting from meetingUserList for id: " + meetingID.ToString());
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Removed Meeting " + meetingID.ToString());
        }

        public void removeComment(int UID, string commentText)
        {
            swatch.Start(0.0);
            General.OutPut("Starting...");
            try
            {
                swatch.Start(0.0);
                command_admintool.CommandText = "DELETE FROM tool_comment WHERE uid=" + UID.ToString() + " AND comment='" + commentText + "'";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
                General.OutPut("Deletet Comment '" + commentText + "' to ID: " + UID.ToString() + " in " + swatch.Time.ToString() + "sek");
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Removed Comment " + commentText + " for " + UID.ToString());
        }

        public void removeRights(int UID)
        {
            swatch.Start(0.0);
            General.OutPut("Deleting Rights...");
            try
            {
                command_admintool.CommandText = "DELETE FROM tool_login WHERE id=" + UID;
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Removed Rights for " + UID);
        }

        public void removeUserMeeting(int UID, int id)
        {
            swatch.Start(0.0);
            General.OutPut("Starting...");
            try
            {
                command_admintool.CommandText = "DELETE FROM tool_meetings_user WHERE uid=" + UID.ToString() + " AND meetingid=" + id.ToString();
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Removed UserMeeting " + id.ToString() + " for " + UID.ToString());
        }

        public void removeUser(int UID)
        {
            swatch.Start(0.0);
            General.OutPut("Starting...");
            try
            {
                command_clan.CommandText = "DELETE FROM clanusers WHERE id='" + UID.ToString() + "'";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Removed User " + UID.ToString());
        }

        public void removeUserSqads(int UID, int id)
        {
            try
            {
                command_clan.CommandText = "DELETE FROM clansquaduser WHERE (user=" + UID.ToString() + ") AND (squad=" + id.ToString() + ")";
                General.OutPut(command_clan.CommandText);
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read()) { }
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            addHistory("Removed Squad " + id.ToString() + " from user " + UID.ToString());
        }

        //Change Methoden
        public void changeMeeting(int UID, int id, string comment, int status)
        {
            swatch.Start(0.0);
            General.OutPut("Starting...");
            try
            {
                command_admintool.CommandText = "UPDATE tool_meetings_user SET comment='" + comment + "',status=" + status.ToString() + " WHERE (meetingid=" + id.ToString() + ") AND (uid=" + UID.ToString() + ")";
                General.OutPut("Changing UserMeetingComment by UID: " + UID.ToString() + " and id: " + id.ToString() + " to: " + comment);
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Changed Meeting for " + UID.ToString() + ", " + id.ToString() + " to " + comment + " with status " + status.ToString());
        }

        public void changeRights(int UID, bool admin)
        {
            swatch.Start(0.0);
            General.OutPut("Changing User Rights...");
            try
            {
                command_admintool.CommandText = "UPDATE tool_login SET admin=" + General.convertBoolToInt(admin) + " WHERE id=" + UID;
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Changed Rights for " + UID + " to Admin " + admin);
        }

        public void changeSquadPosi(int UID, int squadID, int newPosi)
        {
            swatch.Start(0.0);
            General.OutPut("Changing User SquadPosi...");
            try
            {
                command_clan.CommandText = "UPDATE clanuserposis SET posi=" + newPosi + " WHERE (user = " + UID.ToString() + ") AND (squad = " + squadID.ToString() + ")";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Changed SquadPosi for " + UID + " suqad " + squadID + " to " + newPosi);
        }

        public void changeUserLevel(int UID, int newLVL)
        {
            swatch.Start(0.0);
            General.OutPut("Changing User LVL...");
            try
            {
                command_clan.CommandText = "UPDATE clanusers SET level='" + newLVL.ToString() + "' WHERE id='" + UID.ToString() + "'";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Changed Level for " + UID + " to " + newLVL);
        }

        public void changeUserPassword(int UID, string pwd)
        {
            swatch.Start(0.0);
            General.OutPut("Changing User Password...");
            try
            {
                command_clan.CommandText = "UPDATE clanusers SET pwd='" + pwd + "' WHERE id='" + UID.ToString() + "'";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                Reader_clan.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            swatch.Stop();
            General.OutPut("Done in " + swatch.Time + "sek");
            addHistory("Changed Password for " + UID.ToString());
        }

        //Get Methoden
        public string getAwayReason(int awayID)
        {
            string ret = "";
            try
            {
                command_clan.CommandText = "SELECT reason FROM clanaway WHERE (id = " + awayID.ToString() + " )";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    ret = (Reader_clan.GetValue(0).ToString());
                }
                Reader_clan.Close();
                return ret;
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                ret = "<html><body><h1><p> ERROR: " + ex.ToString() + "</p></h1></body></html>";
                return ret;
            }
        }

        public List<CommentView> getComments(int UID)
        {
            try
            {
                List<CommentView> comment = new List<CommentView>();
                command_admintool.CommandText = "SELECT byuid, comment, time FROM tool_comment WHERE uid=" + UID.ToString() + " ORDER BY time";
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    comment.Add(new CommentView { by = getName(int.Parse(Reader_admintool.GetValue(0).ToString())), comment = Reader_admintool.GetValue(1).ToString(), time = General.convertTimeStampToString(double.Parse(Reader_admintool.GetValue(2).ToString()), true) });
                }
                Reader_admintool.Close();
                return comment;
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                return null;
            }
        }

        public string getName(int UID)
        {
            try
            {
                command_clan.CommandText = "SELECT nick FROM clanusers WHERE id=" + UID.ToString();
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                string ret = string.Empty;
                while (Reader_clan.Read())
                {
                    ret = Reader_clan.GetValue(0).ToString();
                }
                Reader_clan.Close();
                return ret;
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                return null;
            }
        }

        public string getOverwriteName(int UID)
        {
            string ret = "";
            try
            {
                command_admintool.CommandText = "SELECT name FROM tool_overwrite WHERE id=" + UID.ToString();
                MySqlDataReader Reader_admintool;
                Reader_admintool = command_admintool.ExecuteReader();
                while (Reader_admintool.Read())
                {
                    ret = Reader_admintool.GetValue(0).ToString();
                }
                Reader_admintool.Close();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
            return ret;
        }

        public int getUserLevel(int UID)
        {
            int ret = 0;
            try
            {
                command_clan.CommandText = "SELECT level FROM clanusers WHERE id=" + UID.ToString();
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    ret = int.Parse(Reader_clan.GetValue(0).ToString());
                }
                Reader_clan.Close();
                return ret;
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                return ret;
            }
        }

        public string getUserMail(int UID)
        {
            try
            {
                command_clan.CommandText = "SELECT email FROM clanusers WHERE id=" + UID.ToString();
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                string ret = string.Empty;
                while (Reader_clan.Read())
                {
                    ret = Reader_clan.GetValue(0).ToString();
                }
                Reader_clan.Close();
                return ret;
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                return "error @ getUserMailByUID";
            }
        }

        public void getUserSqads(System.Windows.Controls.ListView squadView, System.Windows.Controls.ListView userView, int UID)
        {
            try
            {
                userView.Items.Clear();
                command_clan.CommandText = "SELECT squad FROM clansquaduser WHERE user=" + UID.ToString();
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    for (int i = 0; i < squadView.Items.Count; i++)
                    {
                        squadView.SelectedIndex = i;
                        SquadView sv = (SquadView)squadView.SelectedItem;

                        if (int.Parse(Reader_clan.GetValue(0).ToString()) == int.Parse(sv.id))
                        {
                            squadView.Items.Remove(sv);
                            userView.Items.Add(sv);
                        }
                    }
                }
                Reader_clan.Close();
                squadView.Items.Refresh();
                userView.Items.Refresh();
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
            }
        }

        public int getUserSquadPosi(int UID, int Squad)
        {
            int ret = 0;
            try
            {
                command_clan.CommandText = "SELECT clanuserposis.posi FROM clansquaduser INNER JOIN clanuserposis ON clansquaduser.user = clanuserposis.user AND clansquaduser.squad = clanuserposis.squad WHERE (clansquaduser.user = " + UID.ToString() + ") AND (clansquaduser.squad = " + Squad.ToString() + ")";
                MySqlDataReader Reader_clan;
                Reader_clan = command_clan.ExecuteReader();
                while (Reader_clan.Read())
                {
                    ret = (int.Parse(Reader_clan.GetValue(0).ToString()));
                }
                Reader_clan.Close();
                return ret;
            }
            catch (Exception ex)
            {
                General.OutPut(ex.ToString());
                ret = 0;
                return ret;
            }
        }
    }
}