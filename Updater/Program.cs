﻿using System;
using System.Diagnostics;
using System.Net;
using System.Xml;

namespace Updater
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string mainfile = "";
                doc.Load("http://invinion.eu/admintool/UpdateList.xml");

                XmlNodeList fileList = doc.SelectNodes("/AdminTool/FileList/File");
                foreach (XmlNode item in fileList)
                {
                    WebClient wc = new WebClient();
                    Console.WriteLine("Lade Datei: '" + item.Attributes["saveto"].InnerText + "' von: '" + item.Attributes["download"].InnerText);
                    wc.DownloadFile(item.Attributes["download"].InnerText, item.Attributes["saveto"].InnerText);
                    if (item.Attributes["mainfile"].InnerText.Equals("true", StringComparison.OrdinalIgnoreCase))
                    {
                        mainfile = item.Attributes["saveto"].InnerText;
                    }
                }
                Console.WriteLine("Done!");
                Console.WriteLine("Drücke eine Taste um das Programm zu öffnen...");
                Console.ReadKey();
                Process.Start(mainfile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Drücke eine Taste um den Updater zu schliessen...");
                Console.ReadKey();
            }
        }
    }
}